module Forward where

import TypeTrafo


-- |The class 'Differentiate' describes the collection of differentiable
-- expressions.
class Differentiate a where
  diff :: a -> D a
