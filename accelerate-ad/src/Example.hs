module Example where

import Data.Array.Accelerate              as A
import Data.Array.Accelerate.Interpreter
import Data.Array.Accelerate.Trafo.Sharing

elementMul :: Acc (Vector Float) -> Acc (Vector Float) -> Acc (Vector Float)
elementMul xs ys = A.zipWith (*) xs ys

elementMul' :: Acc (Vector (Float, Float)) -> Acc (Vector (Float, Float)) -> Acc (Vector (Float, Float))
elementMul' xs ys = A.zipWith (\x y -> lift (A.fst x * A.fst y, A.fst x * A.snd y + A.snd x * A.fst y)) xs ys

elemAdd :: Acc (Vector Float) -> Acc (Vector Float) -> Acc (Vector Float)
elemAdd xs ys = A.zipWith (+) xs ys
