{-# LANGUAGE TypeFamilies #-}

module TypeTrafo where


-- |The type family 'D' describes how the type of the derivative of an
-- expression depends on the type of the expression itself.
type family D a where
  D Int      = Int
  D Double   = (Double, Double)
  D (a -> b) = D a -> D b

type Example a = a -> D a

exampleF1 :: Example Int
exampleF1 = undefined

exampleF2 :: Example Double
exampleF2 = undefined

exampleF3 :: Example (Double -> Double)
exampleF3 = undefined
